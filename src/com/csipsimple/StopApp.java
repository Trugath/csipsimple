package com.csipsimple;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.csipsimple.api.SipManager;
import com.csipsimple.ui.SipHome;
import com.csipsimple.utils.PreferencesProviderWrapper;
import com.csipsimple.utils.PreferencesWrapper;

public class StopApp extends Activity {

	@Override
	public void onCreate(Bundle icicle) {
			
		final Context context = this;
		new Handler().postDelayed(new Runnable() {
	        @Override
	        public void run() {
	        	
	    		// set the app as killed
	    		PreferencesProviderWrapper prefProviderWrapper = new PreferencesProviderWrapper(context);
	    		prefProviderWrapper.setPreferenceBooleanValue(PreferencesWrapper.HAS_BEEN_QUIT, true);
	    		
	    		try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		
	    		// disconnect
	    		Intent intent = new Intent(SipManager.ACTION_OUTGOING_UNREGISTER);
	    		intent.putExtra(SipManager.EXTRA_OUTGOING_ACTIVITY, new ComponentName(context, SipHome.class));
	    		sendBroadcast(intent);
	        
	    		try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		
	    		// stop the service if it is still running
	    		stopService(new Intent(StopApp.this, com.csipsimple.service.SipService.class));
	    		
	    		try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		
	    		// kill the app
	    		android.os.Process.killProcess( android.os.Process.myPid() );
	    		finish();
	        }
	    }, 0);	
		
		super.onCreate(icicle);		
		this.finish();
	}	
}
